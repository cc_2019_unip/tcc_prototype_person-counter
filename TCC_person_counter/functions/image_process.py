# Arquivo image_process dentro da pasta functions

import cv2
from functions.utils import utils

# Classe responsavel pelo processamento de imagem


class ImageProcess:
    # Função de Pré-Processamento
    @staticmethod
    def pre_processing(frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        return gray

    # Função de Segmentação
    @staticmethod
    def segmentation(frame):
        (retval, th) = cv2.threshold(frame, 20, 255, cv2.THRESH_BINARY_INV)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4, 4))
        opening = cv2.morphologyEx(th, cv2.MORPH_OPEN, kernel, iterations=7)

        return opening

    # Função de Rastreamento
    @staticmethod
    def tracking(frame):
        return cv2.findContours(frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Função de Detecção de Objetos
    @staticmethod
    def detect_persons(persons, contours, frame, src_frame=None):
        i = 0
        bottomBar = utils.get_bottomBar()
        topBar = utils.get_topBar()

        for cnt in contours:
            (x, y, w, h) = cv2.boundingRect(cnt)
            area = cv2.contourArea(cnt)
            if(src_frame is not None):
                cv2.rectangle(src_frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

            # Eliminação de Objetos com área inferior.
            if int(area) > 10000:
                centro = utils.center(x, y, w, h)
                posYCenter = centro[1]
                # Inserir número do index no retangulo
                if(src_frame is not None):
                    cv2.circle(src_frame, centro, 4, (0, 0, 255), -1)

                if len(persons) <= i:
                    persons.append([])
                # Se o centro do objeto estiver dentro da area de contagem
                if posYCenter > topBar and posYCenter < bottomBar:
                    persons[i].append(centro)
                else:
                    persons[i].clear()
                i += 1
        return persons, i
