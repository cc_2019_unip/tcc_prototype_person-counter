# Arquivo utils.py dentro da pasta functions

import cv2
class utils(object):
    """description of class"""
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)   
    
    # Calcular o centro de um retangulo
    @staticmethod  
    def center(x, y, w, h):
        x1 = int(w / 2)
        y1 = int(h / 2)
        cx = x + x1
        cy = y + y1
        return cx, cy

    # Pegar valores da linha central 
    @staticmethod  
    def get_centerBar():
        return 250

    # Pegar valores da linha inferior 
    @staticmethod
    def get_bottomBar():
        offset = 180
        return utils.get_centerBar()+offset

   # Pegar valores da linha superior 
    @staticmethod  
    def get_topBar():
        offset = 180
        return utils.get_centerBar()-offset


#classe utilizada para armanezar os resultados da contabilização.
class Counter():
    total = 0
    descents = 0
    climbs = 0



        
