# Arquivo principal main.py

# Bibliotecas Utilizadas

import cv2
import asyncio
import threading

# Chamada das Funções declaradas em uma Classe

from functions.image_process import ImageProcess
from functions.utils import utils
from functions.utils import Counter

# Videos utilizados para demosntração, passamos o path do arquivo a ser lido e seu nome e extenção.

#cap1 = cv2.VideoCapture("rtsp://192.168.43.92:8080/h264_ulaw.sdp")
cap1 = cv2.VideoCapture("Samples/5.mp4")
backSub = cv2.createBackgroundSubtractorMOG2(history = 259,varThreshold=100, detectShadows=False)

#cap2 = cv2.VideoCapture("Samples/5.mp4")

# Variaveis globais

centerBar = utils.get_centerBar()  # Posicao central
# Distancia entre o eixo(barra cima < centro < barra baixo)
topBar = utils.get_topBar()
# Distancia entre o eixo(barra cima > centro > barra baixo)
bottomBar = utils.get_bottomBar()

counter = Counter()

# Função Principal Responsavel pelo Processamento  e Contabilização


def video_counter(cap, video_name):
    centerBar = utils.get_centerBar()
    topBar = utils.get_topBar()
    bottomBar = utils.get_bottomBar()
    frame_counter = 0
    detects = []
    if (cap.isOpened()):
        while(True):
            # Captura frame a frame

            ret, frame = cap.read()

            # Estabilização dos Frames da Captura

            #cv2.waitKey(20)
            frame_counter += 1

            # Chamada da função de Pre-Processamento


            gray = ImageProcess.pre_processing(frame)
            fgMask = backSub.apply(gray)
            cv2.imshow("fgMask", fgMask)


            # Chamda da função de Segmentação

            opening = ImageProcess.segmentation(gray)
            cv2.imshow("opening", opening)

            # Definindo Região de Interesse conforme posição central da imagem.

            imageWidth = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
            imageHeight = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            cv2.line(frame, (0, centerBar),
                     (imageWidth, centerBar), (255, 0, 0), 3)

            cv2.line(frame, (0, bottomBar),
                     (imageWidth, bottomBar), (255, 255, 0), 2)

            cv2.line(frame, (0, topBar),
                     (imageWidth, topBar), (255, 255, 0), 2)

            # Rastreamento (Detecção de Objetos)

            contours, hierarchy = cv2.findContours(
                opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            (persons, i) = ImageProcess.detect_persons(
                detects, contours, opening, src_frame=frame)

            if i == 0:
                detects.clear()
            else:
                for targetPositions in detects:
                    for (index, position) in enumerate(targetPositions):
                        if targetPositions[index - 1][1] < centerBar and position[1] > centerBar:
                            targetPositions.clear()
                            counter.climbs += 1
                            counter.total += 1

                        elif targetPositions[index - 1][1] > centerBar and position[1] < centerBar:
                            targetPositions.clear()
                            counter.descents += 1
                            counter.total += 1

                        if index > 0:
                            cv2.line(
                                frame, targetPositions[index - 1], position, (0, 0, 255), 3)

                        if index > 5:
                            break

            # Reiniciar o videos se a contabilização estiver completa.

            if(frame_counter == cap.get(cv2.CAP_PROP_FRAME_COUNT)):
                cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                frame_counter = 0

            # Imprimir resultados da Contabilização

            cv2.putText(frame, f'TOTAL: {counter.total}', (10, imageHeight - 60),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
            cv2.putText(frame, f'SUBINDO: {counter.climbs}', (10, imageHeight - 40),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv2.putText(frame, f'DESCENDO: {counter.descents}', (
                10, imageHeight - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            climbs = int(counter.climbs - counter.descents)
            if (climbs < 0):
                climbs = 0
            cv2.putText(frame, f'DENTRO: {climbs}', (10,
                                                     imageHeight - 80), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
            cv2.imshow(video_name, frame)

            # Finalizar a execução apertando a tecla q
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

# Função de Processamento em Paralelo


async def main():
    threading.Thread(target=video_counter, args=(cap1, "video1")).start()
    #threading.Thread(target=video_counter, args=(cap2, "video2")).start()

asyncio.run(main())

# Fechar as janelas após falha.

cv2.destroyAllWindows()
